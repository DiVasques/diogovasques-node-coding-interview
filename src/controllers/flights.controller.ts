import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
    Delete,
} from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    private _flightsService: FlightsService

    constructor() {
        this._flightsService = new FlightsService()
    }

    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await this._flightsService.getFlights(),
        }
    }

    @Put('/:code/passengers')
    async updateFlightStatus(@Param('code') code: string,
        @Body()
        passenger: { id: string }) {
        if (!passenger) {
            return {
                status: 400,
                data: "The passenger cannot be null"
            }
        }
        if (!passenger.id) {
            return {
                status: 400,
                data: "The passenger id cannot be null"
            }
        }
        return {
            status: 200,
            data: await this._flightsService.updateFlightStatus(code, passenger.id),
        }
    }

    @Delete('/:code/passengers/:passengerId')
    async removePassenger(@Param('code') code: string,
        @Param('passengerId') passengerId: string) {
        return {
            status: 200,
            data: await this._flightsService.removePassenger(code, passengerId),
        }
    }

    // add flight
    @Post('')
    async addFlight(
        @Body()
        flight: {
            code: string
            origin: string
            destination: string
            status: string
        }
    ) {
        return {
            status: 200,
            data: await this._flightsService.addFlight(flight),
        }
    }
}

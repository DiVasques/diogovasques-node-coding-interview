export class Database {
    public static _instance: any

    public static getInstance() {
        // subclass must implement this method
    }

    public async getFlights() {
        // subclass must implement this method
    }

    public async updateFlightStatus(code: string, passengerId: string): Promise<number>  {
        throw Error("Method not implemented")
    }

    public async removePassenger(code: string, passengerId: string): Promise<number>  {
        throw Error("Method not implemented")
    }

    public async addFlight(flight: {
        code: string
        origin: string
        destination: string
        status: string
    }) {
        // subclass must implement this method
    }
}

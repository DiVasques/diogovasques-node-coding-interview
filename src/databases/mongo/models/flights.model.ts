import mongoose, { Schema } from 'mongoose'
import { Passenger } from './passenger'

interface Flight {
    code: string
    origin: string
    destination: string
    status: string
    passengers: string[]
}

const schema = new Schema<Flight>(
    {
        code: { required: true, type: String },
        origin: { required: true, type: String },
        destination: { required: true, type: String },
        passengers: { type: [String], default: [] },
        status: String,
    },
    { timestamps: true }
)

export const FlightsModel = mongoose.model('Flights', schema)

import { Database } from '../databases/database_abstract'
import { DatabaseInstanceStrategy } from '../database'

export class FlightsService {
    private readonly _db: Database

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance()
    }

    public async getFlights() {
        return this._db.getFlights()
    }

    public async updateFlightStatus(code: string, passengerId: string) {
        let modifiedCount = await this._db.updateFlightStatus(code, passengerId)
        if (modifiedCount === 0) {
            throw Error("Error updating the passengers list")
        }
    }

    public async removePassenger(code: string, passengerId: string) {
        let modifiedCount = await this._db.removePassenger(code, passengerId)
        if (modifiedCount === 0) {
            throw Error(`Error removing the passenger ${passengerId}`)
        }
    }

    public async addFlight(flight: {
        code: string
        origin: string
        destination: string
        status: string
    }) {
        return this._db.addFlight(flight)
    }
}
